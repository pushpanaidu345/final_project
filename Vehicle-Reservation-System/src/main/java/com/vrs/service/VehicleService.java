package com.vrs.service;

import java.util.List;
import java.util.Optional;

import com.vrs.model.Vehicle;

public interface VehicleService {
	
	public abstract Vehicle save(Vehicle vehicle);

	public abstract List<Vehicle> findAll();

	public abstract Optional<Vehicle> findById(int id);

	public abstract void deleteById(int id);

	public abstract List<Vehicle> findByType(String type);

}
