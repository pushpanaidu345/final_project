package com.vrs.service;

import java.util.List;

import com.vrs.model.User;

public interface UserService {

public abstract	User fetchUserByEmailId(String tempEmail);

public abstract User saveUser(User user);

public abstract User getUserByEmailIdAndPassword(String emailId, String password);

public abstract List<User> findAll();

public abstract User save(User user);

}
