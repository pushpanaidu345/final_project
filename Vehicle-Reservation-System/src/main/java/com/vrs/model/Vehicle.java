package com.vrs.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "VEHICLE")
public class Vehicle {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer vehicleId;
	private String vehicleName;
	private String type;
	private String registrationNo;
	@JsonBackReference
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "userId") // FK

	private User user;

	public Vehicle() {
		super();
	}

	public Vehicle(Integer vehicleId, String vehicleName, String type, String registrationNo) {
		super();
		this.vehicleId = vehicleId;
		this.vehicleName = vehicleName;
		this.type = type;
		this.registrationNo = registrationNo;
	}

	public Vehicle(String vehicleName, String type, String registrationNo) {
		super();
		this.vehicleName = vehicleName;
		this.type = type;
		this.registrationNo = registrationNo;
	}

	public Integer getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(Integer vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getVehicleName() {
		return vehicleName;
	}

	public void setVehicleName(String vehicleName) {
		this.vehicleName = vehicleName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getRegistrationNo() {
		return registrationNo;
	}

	public void setRegistrationNo(String registrationNo) {
		this.registrationNo = registrationNo;
	}

}
